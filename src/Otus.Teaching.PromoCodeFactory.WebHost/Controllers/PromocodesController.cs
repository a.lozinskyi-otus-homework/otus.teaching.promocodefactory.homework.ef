﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Preference> preferenceRepository)
        {
            PromocodeRepository = promocodeRepository;
            PreferenceRepository = preferenceRepository;
        }

        public IRepository<PromoCode> PromocodeRepository { get; }

        public IRepository<Preference> PreferenceRepository { get; }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Список текущих выданных промокодов</returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await PromocodeRepository.GetAllAsync();
            return promocodes
                .Select(
                    p =>
                        new PromoCodeShortResponse
                        {
                            BeginDate = p.BeginDate.ToString("dd.MM.yyyy"),
                            Code = p.Code,
                            EndDate = p.EndDate.ToString("dd.MM.yyyy"),
                            Id = p.Id,
                            PartnerName = p.PartnerName,
                            ServiceInfo = p.ServiceInfo
                        })
                .ToList();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns>Результат выдачи промокодов</returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await PreferenceRepository.GetAllAsync();
            var preference = 
                preferences.FirstOrDefault(
                    p => string.Equals(p.Name, request.Preference, StringComparison.OrdinalIgnoreCase));
            if (preference == null)
            {
                return NotFound(request);
            }

            await PromocodeRepository.AddRangeAsync(
                preference.CustomersPreferences.Select(
                    cp =>
                        new PromoCode
                        {
                            Id = Guid.NewGuid(),
                            ServiceInfo = request.ServiceInfo,
                            Preference = preference,
                            PartnerName = request.PartnerName,
                            Code = request.PromoCode,
                            Customer = cp.Customer
                        }));
            return Ok();
        }
    }
}