﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
    {
        
        public PreferencesController(IRepository<Preference> preferencesRepository)
        {
            PreferencesRepository = preferencesRepository;
        }

        private IRepository<Preference> PreferencesRepository { get; }

        /// <summary>
        /// Получить все доступные предпочтения
        /// </summary>
        /// <returns>Список возможных предпочтений</returns>
        [HttpGet]
        public async Task<IEnumerable<PreferenceShortResponse>> GetPreferencesAsync()
        {
            var preferences = await PreferencesRepository.GetAllAsync();
            return preferences.Select(
                p =>
                    new PreferenceShortResponse
                    {
                        Id = p.Id,
                        Name = p.Name
                    });
        }
    }
}