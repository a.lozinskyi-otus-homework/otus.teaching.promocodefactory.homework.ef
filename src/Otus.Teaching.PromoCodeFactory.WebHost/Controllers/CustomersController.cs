﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        public CustomersController(IRepository<Customer> customersRepository, IRepository<CustomerPreference> customerPreferencesRepository)
        {
            CustomersRepository = customersRepository;
            CustomerPreferencesRepository = customerPreferencesRepository;
        }

        private IRepository<Customer> CustomersRepository { get; }

        private IRepository<CustomerPreference> CustomerPreferencesRepository { get; }


        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns>Список клиентов</returns>
        public async Task<IEnumerable<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await CustomersRepository.GetAllAsync();
            return customers.Select(
                c =>
                    new CustomerShortResponse
                    {
                        Id = c.Id,
                        Email = c.Email,
                        FirstName = c.FirstName,
                        LastName = c.LastName
                    })
                .ToList();
        }
        
        /// <summary>
        /// Получение клиента по Id
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns>Клиент с его промокодами</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await CustomersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            return 
                new CustomerResponse
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    PromoCodes = 
                        customer.PromoCodes
                            .Select(
                                pc =>
                                new PromoCodeShortResponse
                                {
                                    Id = pc.Id,
                                    BeginDate = pc.BeginDate.ToString("dd.MM.yyyy"),
                                    EndDate = pc.EndDate.ToString("dd.MM.yyyy"),
                                    Code = pc.Code,
                                    PartnerName = pc.PartnerName,
                                    ServiceInfo = pc.ServiceInfo
                                })
                            .ToList(),
                    Preferences = 
                        customer.CustomersPreferences
                            .Select(
                                cp =>
                                    new PreferenceShortResponse
                                    {
                                        Id = cp.Id,
                                        Name = cp.Preference?.Name
                                    })
                            .ToList()
                };
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return NoContent();
            }

            // ToDo: Use Mapper
            var customerId = Guid.NewGuid();
            var customer = 
                new Customer
                {
                    CustomersPreferences = 
                        request.PreferenceIds
                            .Select(
                                pi =>
                                    new CustomerPreference
                                    {
                                        PreferenceId = pi,
                                        Id = Guid.NewGuid(),
                                        CustomerId = customerId
                                    }).ToList(),
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Id = customerId
                };
            await CustomersRepository.AddAsync(customer);
            return Ok();
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null)
            {
                return NoContent();
            }

            // ToDo: Use Mapper
            var customer =
                new Customer
                {
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Id = id
                };
            await CustomersRepository.UpdateAsync(customer);

            var existingPreferences = (await CustomerPreferencesRepository.GetAllAsync()).Where(ep => ep.CustomerId == id).ToList();
            var existingPreferencesToDelete = 
                existingPreferences.Where(ep => request.PreferenceIds.All(pi => pi != ep.PreferenceId));
            await CustomerPreferencesRepository.DeleteRangeAsync(existingPreferencesToDelete);
            
            var existingPreferencesIdsToAdd = 
                request.PreferenceIds.Where(pi => existingPreferences.All(ep => ep.PreferenceId != pi));
            await CustomerPreferencesRepository.AddRangeAsync(
                existingPreferencesIdsToAdd.Select(
                    epita =>
                        new CustomerPreference
                        {
                            Id = Guid.NewGuid(),
                            CustomerId = id,
                            PreferenceId = epita
                        }));
            return Ok();
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customerToDelete = await CustomersRepository.GetByIdAsync(id);
            if (customerToDelete == null)
            {
                return NotFound();
            }

            await CustomersRepository.DeleteAsync(customerToDelete);
            return Ok();
        }
    }
}