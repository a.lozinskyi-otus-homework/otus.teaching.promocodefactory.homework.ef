﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        public EfRepository(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        private DataContext DataContext { get; }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DataContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await DataContext.Set<T>().FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task AddAsync(T entity)
        {
            await DataContext.Set<T>().AddAsync(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await DataContext.Set<T>().AddRangeAsync(entities);
            await DataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            DataContext.Update(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            DataContext.Set<T>().Remove(entity);
            await DataContext.SaveChangesAsync();
        }

        public async Task DeleteRangeAsync(IEnumerable<T> entities)
        {
            DataContext.RemoveRange(entities);
            await DataContext.SaveChangesAsync();
        }
    }
}