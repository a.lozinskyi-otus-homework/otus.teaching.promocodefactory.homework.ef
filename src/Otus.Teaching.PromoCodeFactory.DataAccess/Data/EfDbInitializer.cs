﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer
    {
        public EfDbInitializer(DataContext dataContext)
        {
            DataContext = dataContext;
        }

        private DataContext DataContext { get; }

        public void InitializeDb()
        {
            // Убраля для миграции
            // DataContext.Database.EnsureDeleted();
            // DataContext.Database.EnsureCreated();

            DataContext.AddRange(FakeDataFactory.Roles);
            DataContext.AddRange(FakeDataFactory.Employees);
            DataContext.AddRange(FakeDataFactory.Preferences);
            DataContext.AddRange(FakeDataFactory.Customers);
            DataContext.AddRange(FakeDataFactory.CustomersPreferences);
            DataContext.AddRange(FakeDataFactory.PromoCodes);

            DataContext.SaveChanges();
        }
    }
}
