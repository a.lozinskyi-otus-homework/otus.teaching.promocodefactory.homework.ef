﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        public string Code { get; set; }

        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(50)]
        public string PartnerName { get; set; }

        public Guid? PartnerManagerId { get; set; }

        public virtual Employee PartnerManager { get; set; }

        public Guid? PreferenceId { get; set; }
        
        public virtual Preference Preference { get; set; }

        public virtual Customer Customer { get; set; }

        public Guid CustomerId { get; set; }
    }
}